# 3DS-WAV-Player

Test application for loading a WAV file into memory and playing it.

# Credits
 - cheuble - much of the code here is based of the sound code in NAMELESS (https://github.com/cheuble/NAMELESS/blob/master/source/sound.cpp)
