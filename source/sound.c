#include <stdio.h>
#include <3ds.h>
#include <string.h>

#include "sound.h"

Result loadWav(const char* path, sound *snd, int channel, bool loop) {
	if ( (channel < 0) || (channel > 23) ) {
		fprintf(stderr, "error: channel passed must be valid (0-23)\n");
		return 1;
	}
	
	u32 sampleRate;
	u16 channels;
	u16 bitsPerSample;
	
	snd->data = NULL;
	
	FILE *fp = fopen(path, "rb");
	if (!fp) {
		fprintf(stderr, "error: invalid file path\n");
		return 1;
	}
	printf("path exists, file opened succesfully\n");
	
	// check magic header
	char magic[4];
	fread(magic, 1, 4, fp);
	if ( (magic[0] != 'R') && (magic[1] != 'I') && (magic[2] != 'F') && (magic[3] != 'F') ) {
		fprintf(stderr, "error: not a WAV file\n");
		return 1;
	}
	printf("'RIFF' header found in WAV\n");
	
	// obtain size of data
	fseek(fp, 0, SEEK_END);
	snd->dataSize = ftell(fp);
	printf("data size: %d\n", (int)snd->dataSize);
	rewind(fp);
	
	// obtain number of channels
	printf("rewound\n");
	fseek(fp, 22, SEEK_SET);
	printf("preparing to read...\n");
	fread(&channels, 2, 1, fp);
	printf("channels: %d\n", (int)channels);
	rewind(fp);
	
	// obtain the sample rate
	fseek(fp, 24, SEEK_SET);
	fread(&sampleRate, 4, 1, fp);
	printf("sample rate: %d\n", (int)sampleRate);
	rewind(fp);
	
	// obtain bits per sample
	fseek(fp, 34, SEEK_SET);
	fread(&bitsPerSample, 2, 1, fp);
	printf("bits per sample: %d\n", (int)bitsPerSample);
	rewind(fp);
	
	// allocate memory and read sound data
	snd->data = (u8*)(linearAlloc(snd->dataSize));
	fseek(fp, 44, SEEK_SET);
	fread(&snd->data, 1, snd->dataSize, fp);
	printf("sound data loaded into memory\n");
	
	fclose(fp);
	printf("file closed\n");
	
	// check to see that WAV file is valid
	if ( (snd->dataSize == 0) || !( (channels == 1) || (channels == 2) ) || !( (bitsPerSample == 8) || (bitsPerSample == 16) ) ){
		fprintf(stderr, "error: WAV file is invalid\n");
		return 1;
	}
	printf("WAV file is valid\n");
	
	snd->dataSize /= 2;
	
	// set format
	u16 ndspFormat;
	if (bitsPerSample == 8) {
		ndspFormat = (channels == 1) ? NDSP_FORMAT_MONO_PCM8 : NDSP_FORMAT_STEREO_PCM8;
	} else {
		ndspFormat = (channels == 1) ? NDSP_FORMAT_MONO_PCM16 : NDSP_FORMAT_STEREO_PCM16;
	}
	printf("ndspFormat set to %d\n", ndspFormat);
	
	// prepare channel
	ndspChnReset(channel);	
	printf("channel reset\n");
	ndspChnSetInterp(channel, NDSP_INTERP_NONE);
	printf("interpolation set\n");
	ndspChnSetRate(channel, (float) sampleRate);
	printf("sample rate set\n");
	ndspChnSetFormat(channel, ndspFormat);
	printf("channel format set\n");
	
	memset(&snd->wbuf, 0, sizeof(snd->wbuf));
	
	// prepare sound
	snd->wbuf.data_vaddr = (u32*)(snd->data);
	printf("sound data loaded in wbuf\n");
	snd->wbuf.looping = loop;
	printf("set looping\n");
	snd->wbuf.status = NDSP_WBUF_FREE;
	printf("set wbuf status\n");
	snd->wbuf.nsamples = snd->dataSize / (bitsPerSample >> 3);
	printf("set nsamples to %d\n", (int)snd->wbuf.nsamples);
	snd->channel = channel;
	printf("set channel (finally)\n");
	
	printf("WAV file loaded successfully\n");
	
	return 0;
}

void freeSound(sound *snd) {
	snd->wbuf.data_vaddr = NULL;
	snd->wbuf.looping = false;
	snd->wbuf.status = 0;
	snd->wbuf.nsamples = 0;
	
	snd->data = NULL;
	snd->dataSize = 0;
	
	ndspChnWaveBufClear(snd->channel);
}

Result playSound(sound *snd) {
	Result res = DSP_FlushDataCache(snd->data, snd->dataSize);
	ndspChnWaveBufAdd(snd->channel, &snd->wbuf);
	
	return res;
}

void stopSound(sound *snd) {
	ndspChnWaveBufClear(snd->channel);
}
