#include <stdio.h>
#include <3ds.h>

#include "sound.h"

int main()
{
	gfxInitDefault();
	//gfxSet3D(true); // uncomment if using stereoscopic 3D

	consoleInit(GFX_TOP, NULL);
	dspInit();

	sound snd;
	Result musicLoaded = loadWav("/gasx3.wav", &snd, 0, true);
	if (musicLoaded != 0) {
		fprintf(stderr, "The WAV file could not be loaded\n");
		goto exit;
	}

	printf("Music loaded.\n \
			Press the A Button to start playback.\n \
			Press the B Button to stop playback.\n");
			
	exit:
	printf("Press the START Button to exit.\n");
	
	// Main loop
	while (aptMainLoop())
	{
		gspWaitForVBlank();
		hidScanInput();

		// Your code goes here

		u32 kUp = hidKeysUp();
		switch (kUp) {
			case (KEY_START):
				goto quit;
				break;
			case (KEY_A):
				if (musicLoaded == 0) {
					Result res = playSound(&snd);
					if (res != 0) {
						printf("Playback failed, Result: %x\n", (unsigned int) res);
					} else {
						printf("Playback should be starting...\n");
					}
				}
				break;
			case (KEY_B):
				if (musicLoaded == 0) {
					stopSound(&snd);
					printf("Stopping playback...\n");
				}
				break;
			default:
				break;
		};

		// Flush and swap framebuffers
		gfxFlushBuffers();
		gfxSwapBuffers();
	}

	quit:	
	freeSound(&snd);

	dspExit();
	gfxExit();
	return 0;
}
