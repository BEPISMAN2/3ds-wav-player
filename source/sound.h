#ifndef SOUND_H
#define SOUND_H

typedef struct {
	ndspWaveBuf wbuf;
	int channel;
	u32 dataSize;
	u8* data;
} sound;

Result loadWav(const char* path, sound *snd, int channel, bool loop);
void freeSound(sound *snd);
Result playSound(sound *snd);
void stopSound(sound *snd);

#endif
